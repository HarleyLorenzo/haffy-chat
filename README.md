# Haffy Chat

Haffy Chat is an encrypted terminal chat client. It is still in very early alpha but will be utilizing many various encryption methods of the communicating party's choosing. As more development is made and features are stable and finalized, the README will become more robust.

## Getting Started

### Dependencies
`libsodium>=1.0.12`
### Compilation
1. You will need cmake>=3.1.0, all above dependencies, and a compiler that supports C++14
2. Navigate to the project root directory and make your makefiles with `cmake .`
3. Then `make` to build the project. The binary executable will be placed as `./bin/haffychat`
