#ifndef CURVE25519_HXX
#   define CURVE25519_HXX

#include <array>
#include <exception>
#include <sodium.h>

typedef std::array<unsigned char, crypto_kx_SECRETKEYBYTES>
    curve25519_secret_key;
typedef std::array<unsigned char, crypto_kx_PUBLICKEYBYTES>
    curve25519_public_key;
typedef std::array<unsigned char, crypto_sign_SEEDBYTES> curve25519_seed;
typedef std::array<unsigned char, crypto_kx_SESSIONKEYBYTES>
    curve25519_session_key;

class e_curve25519: public std::exception
{
public:
    virtual const char* what() const noexcept =0;
};

class e_curve25519_invalid_public: public e_curve25519
{
public:
    virtual const char* what() const noexcept;
};

class curve25519
{
protected:
    curve25519_secret_key local_key_secret;
    curve25519_public_key local_key_public;

public:
    curve25519_public_key get_local_key_public(void) const;
    void new_local_keys (void) noexcept;
    std::array<unsigned char, crypto_kx_SESSIONKEYBYTES*2>
        session_keys (curve25519 const &) const;
    void set_local_key_public (curve25519_public_key const &);
    void swap(curve25519 const &) noexcept;
};

#endif // CURVE25519_HXX
