/* This is the entry-point file for the haffy-chat binary executable */

#include "haffy-chat.hxx"
#include "haffy-chat-config.h"
#include "chat.hxx"
#include <iostream>
#include <string>
#include <vector>
#include <sodium.h>

int main (int argc, char **argv)
{
    global::boolFlags.fill(false);
    global::intFlags.fill(0);

    /* Parse our arguments */
    if (argc > 1)
    {
        /* Store our arguments in a string object for easy parsing */
        std::vector<std::string> str_argv (argc-1);
        for (short i = 0; i < argc-1; i++)
        {
            str_argv.at(i) = argv[i+1];
        }
        /* Begin parsing */
        for (short i = 0; i < argc-1; i++)
        {
            /* If -v or --verbose*/
            if (str_argv.at(i) == "-v" || str_argv.at(i) == "--verbose")
            {
                if (global::boolFlags[global::verbose] == false)
                {
                    global::boolFlags[global::verbose] = true;
                }
                else
                {
                    std::cerr << "Verbosity already enabled! Ignoring!"
                        << std::endl;
                }
            }
            else if (str_argv.at(i) == "--version")
            {
                std::cout << "HaffyChat's running version is: "
                    << global::version << std::endl;
            }
            else
            {
                std::cerr << "Unknown argument: \"" << str_argv.at(i)
                    << "\" Ignoring!" << std::endl;
            }
        }
    }
    if (global::boolFlags[global::verbose] == true)
    {
        std::cout << "Initializing sodium." << std::endl;
    }
    if (sodium_init() == -1)
    {
        std::cerr << "Sodium could not be initialized! Exiting!" << std::endl;
        return -1;
    }
    if (crypto_aead_aes256gcm_is_available() == 0) {
        std::cerr << "CPU Does not support aes256gcm! Exiting!" << std::endl;
        return -1;
    }
    return initialize_chat();
}
