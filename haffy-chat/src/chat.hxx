#ifndef CHAT_H
#   define CHAT_H

#include "aes-256-gcm.hxx"
#include <fstream>
#include <string>

/*** Globals ***/

const unsigned int encrypt_size = 4*1024;
const unsigned int decrypt_size = encrypt_size +
    crypto_aead_aes256gcm_NPUBBYTES + crypto_aead_aes256gcm_ABYTES;

/*** Functions ***/

std::vector<unsigned char> file_read(std::string const &,
    std::streampos const &, unsigned long long);

void file_write(std::string const &, std::vector<unsigned char> const &);

void file_encrypt(aes_256_gcm_base64 &, std::string const &,
    std::string const &);

void file_decrypt(aes_256_gcm_base64 &, std::string const &,
    std::string const &);

int curve25519_aes_chat();

int aes_chat(void);

int initialize_chat(void);

#endif /* CHAT_H */
