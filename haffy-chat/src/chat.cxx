/* Chat.cxx contains all the code for the encrypted chat client */

#include "chat.hxx"
#include "aes-256-gcm.hxx"
#include "base64.hxx"
#include "curve25519.hxx"
#include <algorithm>
#include <array>
#include <exception>
#include <fstream>
#include <future>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <sodium.h>

/*** Functions ***/

std::vector<unsigned char> file_read(std::string const &path,
    std::streampos const &pos, unsigned long long len)
{
    /* Try to open the file */
    std::ifstream file(path, std::ios::binary);
    if (!file.good())
    {
        std::string string = "File: \"";
        string += path;
        string += "\" is unable to be opened";
        throw(std::logic_error(string));
    }
    /* Seek to the position we're given, read the data into ret and return */
    file.seekg(pos);
    std::vector<unsigned char> ret(len);
    file.read(reinterpret_cast<char *>(ret.data()), len);
    file.close();
    return ret;
}

void file_write(std::string const &path,
    std::vector<unsigned char> const &buffer)
{
    /* Try to open the file and write the buffer we were given to it in
     * append mode */
    std::ofstream file(path, std::ios::binary | std::ios::app);
    if (!file.good())
    {
        std::string string = "File: \"";
        string += path;
        string += "\" is unable to be opened";
        throw(std::logic_error(string));
    }
    file.write(reinterpret_cast<const char *>(buffer.data()), buffer.size());
    file.close();
}

void file_decrypt(aes_256_gcm_base64 &aes_client, std::string const &read_path,
    std::string const &write_path)
{
    /* Try to open both files and set the appropriate counters */
    std::fstream file(read_path, std::ios::binary | std::ios::in);
    if (!file.good())
    {
        std::string string = "File: \"";
        string += read_path;
        string += "\" is unable to be opened";
        throw(std::logic_error(string));
    }
    auto read_pos = file.tellg();
    file.seekg(0, std::ios::end);
    auto end = file.tellg();
    file.seekg(read_pos);
    file.close();
    if ((end - read_pos) == 0)
    {
        std::string string = "File: \"";
        string += read_path;
        string += "\" is empty";
        throw(std::logic_error(string));
    }
    file.open(write_path, std::ios::binary | std::ios::out);
    if (!file.good())
    {
        std::string string = "File: \"";
        string += write_path;
        string += "\" is unable to be opened";
        throw(std::logic_error(string));
    }
    /* With the file we write to, we overwrite it */
    file << "";
    file.close();
    unsigned int len;
    /* Start the first read with maximum size of 4 KB*/
    if ((end - read_pos) > decrypt_size)
    {
        len = decrypt_size;
    }
    else
    {
        len = end - read_pos;
    }
    auto read_fut = std::async(std::launch::async, file_read,
        read_path, read_pos, len);
    std::vector<unsigned char> out_buffer = aes_client.decrypt(read_fut.get());
    read_pos += len;
    /* Start the first write */
    auto write_fut = std::async(std::launch::async, file_write, write_path,
        out_buffer);
    /* Continue reading and writing until we're finished */
    while (read_pos < end)
    {
        if ((end - read_pos) > decrypt_size)
        {
            len = decrypt_size;
        }
        else
        {
            len = end - read_pos;
        }
        read_fut = std::async(std::launch::async, file_read,
            read_path, read_pos, len);
        out_buffer = aes_client.decrypt(read_fut.get());
        write_fut.get();
        write_fut = std::async(std::launch::async, file_write, write_path,
            out_buffer);
        read_pos += len;
    }
    /* There will always be a write_fut to get here */
    write_fut.get();
}

void file_encrypt(aes_256_gcm_base64 &aes_client,
    std::string const &read_path, std::string const &write_path)
{
    /* Try to open both files and set the appropriate counters */
    std::fstream file(read_path, std::ios::binary | std::ios::in);
    if (!file.good())
    {
        std::string string = "File: \"";
        string += read_path;
        string += "\" is unable to be opened";
        throw(std::logic_error(string));
    }
    auto read_pos = file.tellg();
    file.seekg(0, std::ios::end);
    auto end = file.tellg();
    file.seekg(read_pos);
    file.close();
    if ((end - read_pos) == 0)
    {
        std::string string = "File: \"";
        string += read_path;
        string += "\" is empty";
        throw(std::logic_error(string));
    }
    file.open(write_path, std::ios::binary | std::ios::out);
    if (!file.good())
    {
        std::string string = "File: \"";
        string += write_path;
        string += "\" is unable to be opened";
        throw(std::logic_error(string));
    }
    /* With the file we write to, we overwrite it */
    file << "";
    file.close();
    unsigned int len;
    /* Start the first read with maximum size of 4 KB */
    if ((end - read_pos) > encrypt_size)
    {
        len = encrypt_size;
    }
    else
    {
        len = end - read_pos;
    }
    auto read_fut = std::async(std::launch::async, file_read,
        read_path, read_pos, len);
    std::vector<unsigned char> out_buffer = aes_client.encrypt(read_fut.get());
    read_pos += len;
    /* Start the first write */
    auto write_fut = std::async(std::launch::async, file_write, write_path,
        out_buffer);
    /* Continue reading and writing until we're finished */
    while (read_pos < end)
    {
        if ((end - read_pos) > encrypt_size)
        {
            len = encrypt_size;
        }
        else
        {
            len = end - read_pos;
        }
        read_fut = std::async(std::launch::async, file_read,
            read_path, read_pos, len);
        out_buffer = aes_client.encrypt(read_fut.get());
        write_fut.get();
        write_fut = std::async(std::launch::async, file_write, write_path,
            out_buffer);
        read_pos += len;
    }
    /* There will always be a write_fut to get here */
    write_fut.get();
}

void curve25519_aes_chat_help(std::string const &help)
{
    if (help.length() == 0)
    {
        std::cout << "-----Haffy Chat Commands-----" << std::endl <<
            std::endl << ".help:\t\tDisplay this help text" <<
            std::endl << ".quit:\t\tQuit the program" <<
            std::endl << ".send:\t\tEncrypt a message for your" <<
            "chat partner" <<

            std::endl << ".encrypt:\tEncrypt a file for your partner" <<
            std::endl << ".decrypt:\tDecrypt a file sent by your partner" <<
            std::endl << std::endl << "For more information on a command " <<
            "use .help [command]" <<

            std::endl << std::endl;
    }
    else
    {
        if ((help.compare(0, 7, "encrypt") == 0) ||
            (help.compare(0, 8, ".encrypt") == 0))
        {
            std::cout << ".encrypt:" << std::endl <<
                "Encrypt a file for your chat partner using your " <<
                "key generated in this session." << std::endl << std::endl <<
                "Usage: .encrypt [File To Encrypt] " <<
                "[File To Save The Ciphertext]" << std::endl << std::endl;
        }
        else if ((help.compare(0, 7, "decrypt") == 0) ||
            (help.compare(0, 8, ".decrypt") == 0))
        {
            std::cout << ".decrypt:" << std::endl <<
                "Decrypt a file for your chat partner using your " <<
                "key generated in this session." << std::endl << std::endl <<
                "Usage: .encrypt [File To Decrypt] " <<
                "[File To Save The Plaintext]" << std::endl << std::endl;
        }
        else
        {
            std::cout << "Unkown help page: " << help << std::endl <<
                std::endl;
        }
    }
}

int curve25519_aes_chat()
{
    curve25519 c25519_local_client, c25519_remote_client;
    std::string input;
    base64 base64_buffer;

    /* Generate new local keys and output it in base64 */
    c25519_local_client.new_local_keys();
    std::vector<unsigned char> arg (crypto_kx_PUBLICKEYBYTES);
    curve25519_public_key tmp_key =
        c25519_local_client.get_local_key_public();
    std::copy(tmp_key.cbegin(), tmp_key.cend(), arg.begin());
    base64_buffer.encode(arg);
    std::cout << "Your public key is: " << base64_buffer.string() << std::endl;
    base64_buffer.clear();
    /* Acquire a valid public key for the partner */
    bool valid_key = false;
    while (!valid_key)
    {
        std::cout << "Enter your partner's public key now: " << std::endl <<
            "> ";
        std::getline(std::cin, input);
        base64_buffer = input;
        try
        {
            std::vector<unsigned char> decoded = base64_buffer.decode();
            if (decoded.size() == crypto_kx_PUBLICKEYBYTES)
            {
                std::array<unsigned char, crypto_kx_PUBLICKEYBYTES> arg;
                std::copy(decoded.cbegin(), decoded.cend(), arg.begin());
                c25519_remote_client.set_local_key_public(arg);
                valid_key = true;
                base64_buffer.clear();
            }
            else
            {
                base64_buffer.clear();
                throw(e_curve25519_invalid_public());
            }
        }
        catch (e_curve25519_invalid_public const &e)
        {
            std::cerr << "Invalid key! Could not initialize chat!" <<
                std::endl << std::endl;
        }
        catch (e_invalid_base64 const &e)
        {
            std::cerr << "Invalid key! Could not initialize chat!" <<
                std::endl << std::endl;
        }
    }
    /* Generate our key pair */
    aes_256_gcm_base64 local_client, remote_client;
    try
    {
        std::array<unsigned char, crypto_kx_SESSIONKEYBYTES*2> keys =
            c25519_local_client.session_keys(c25519_remote_client);
        curve25519_session_key local_key;
        curve25519_session_key remote_key;
        std::copy(keys.cbegin(), keys.cbegin() + crypto_kx_SESSIONKEYBYTES,
            local_key.begin());
        std::copy(keys.cbegin() + crypto_kx_SESSIONKEYBYTES, keys.cend(),
            remote_key.begin());
        local_client.new_key(local_key);
        remote_client.new_key(remote_key);
    }
    catch (std::exception const &e)
    {
        std::cerr << "Could not initialize chat! Exception caught: " <<
            e.what() << std::endl;
        return -1;
    }
    /* Initialize chat */
    bool exit = false;
    std::string arg_string;
    std::cout <<
    "----Chat initialized----" << std::endl <<
    "Enter in your partner's message to decode or use .send [message] to " <<
    "encode your own message. Happy chatting!" << std::endl << std::endl;
    while (!exit)
    {
        /* Get user input and deicde what to do with it */
        std::cout << "> ";
        std::getline(std::cin, input);
        std::cout << std::endl;
        /* .quit will quit the program */
        if (input.compare(0, 1, ".") == 0)
        {
            if (input.compare(0, 5, ".quit") == 0)
            {
                exit = true;
            }
            /* .help displays help text */
            else if (input.compare(0, 5, ".help") == 0)
            {
                if (input.length() >= 7)
                {
                    curve25519_aes_chat_help(input.substr(6));
                }
                else
                {
                    curve25519_aes_chat_help("");
                }
            }
            /* .send will attempt to encrypt the string after and display it in
             * encrypted base64 text */
            else if (input.compare(0, 6, ".send ") == 0)
            {
                if (input.length() >= 7)
                {
                    try
                    {
                        arg_string = input.substr(6);
                        std::cout << remote_client.encrypt(arg_string).string() <<
                            std::endl << std::endl;
                    }
                    catch (e_aes_encryption_error const &e)
                    {
                        std::cerr << "Encryption failed!" << std::endl;
                    }
                }
            }
            else if (input.compare(0, 9, ".encrypt ") == 0 ||
                input.compare(0, 9, ".decrypt ") == 0)
            {
                /* Proceduraly encrypt/decrypt based on which argument
                 * we received */
                bool encrypt = false;
                if (input.compare(0, 9, ".encrypt ") == 0)
                {
                    encrypt = true;
                }
                unsigned int it = 9;
                unsigned int len = input.length();
                bool quoted_path = false;
                /* If the path begins with a quote we read it differently */
                if (input.compare(9, 1, "\'")  == 0 ||
                    input.compare(9, 1, "\"") == 0)
                {
                    quoted_path = true;
                    it++;
                }
                bool end = false;
                /* The two arguments we eventually pass and a pointer */
                std::string file_read("");
                std::string file_write("");
                std::string *file = &file_read;
                while (!end)
                {
                    /* If we reached the end of input break out of the loop */
                    if (it >= len)
                    {
                        end = true;
                        break;
                    }
                    /* If we reached the end of the path break out */
                    if (quoted_path)
                    {
                        if (input.compare(it, 1, "\'") == 0 ||
                            input.compare(it, 1, "\"") == 0)
                        {
                            end = true;
                            it++;
                            break;
                        }
                    }
                    else
                    {
                        if (input.compare(it, 1, " ") == 0)
                        {
                            end = true;
                            break;
                        }
                    }
                    /* Otherwise add the character to our string and
                     * increment */
                    *file += input.at(it);
                    it++;
                }
                /* Ignore any spaces between paths */
                while (input.compare(it, 1, " ") == 0)
                {
                    it++;
                }
                /* If by then we reached the end then we know that the
                 * second path does not exist */
                if (it >= len)
                {
                    std::cerr << "Unknown second argument provided!" <<
                        std::endl << std::endl;
                }
                else
                {
                    /* Reset and set up for the second argument */
                    end = false;
                    file = &file_write;
                    if (input.compare(9, 1, "\'")  == 0 ||
                        input.compare(9, 1, "\"") == 0)
                    {
                        quoted_path = true;
                        it++;
                    }
                    else
                    {
                        quoted_path = false;
                    }
                    while (!end)
                    {
                        /* If we reached the end of input break out of the
                         * loop */
                        if (it >= len)
                        {
                            end = true;
                            break;
                        }
                        /* If we reached the end of the path break out */
                        if (quoted_path)
                        {
                            if (input.compare(it, 1, "\'") == 0 ||
                                input.compare(it, 1, "\"") == 0)
                            {
                                end = true;
                                it++;
                                break;
                            }
                        }
                        else
                        {
                            if (input.compare(it, 1, " ") == 0)
                            {
                                end = true;
                                break;
                            }
                        }
                        /* Otherwise add the character to our string and
                         * increment */
                        *file += input.at(it);
                        it++;
                    }
                    /* Encrypt or decrypt proceduraly */
                    if (encrypt)
                    {
                        try
                        {
                            file_encrypt(remote_client, file_read, file_write);
                            std::cout << "Encryption successful!" <<
                                std::endl << std::endl;
                        }
                        catch (std::logic_error const &e)
                        {
                            std::cout << "Unable to encrypt file! Error: " <<
                                e.what() << std::endl << std::endl;
                        }
                    }
                    else
                    {
                        try
                        {
                            file_decrypt(local_client, file_read, file_write);
                            std::cout << "Decryption successful!" <<
                                std::endl << std::endl;
                        }
                        catch (std::logic_error const &e)
                        {
                            std::cout << "Unable to decrypt file! Error: " <<
                                e.what() << std::endl << std::endl;
                        }
                    }
                }
            }
            else
            {
                std::cerr << "Invalid command: " << input << std::endl <<
                    std::endl;
            }
        }
        /* If we receive a string of text without the command prefix, assume
         * it's a message to decrypt */
        else
        {
            try
            {
                base64_buffer = input;
                std::cout << local_client.decrypt(base64_buffer) << std::endl;
                base64_buffer.clear();
            }
            catch (e_aes_decryption_error const &e)
            {
                std::cerr << "Decryption failed!" << std::endl;
            }
            catch (e_invalid_base64 const &e)
            {
                std::cerr << "Decryption failed!" << std::endl;
            }
        }
    }
    return 0;
}

void aes_chat_help(std::string const &help)
{
    if (help.length() == 0)
    {
        std::cout << "-----Haffy Chat Commands-----" << std::endl <<
            std::endl << ".help:\t\tDisplay this help text" <<
            std::endl << ".quit:\t\tQuit the program" <<
            std::endl << ".send:\t\tEncrypt a message for your" <<
            "chat partner" <<

            std::endl << ".encrypt:\tEncrypt a file for your partner" <<
            std::endl << ".decrypt:\tDecrypt a file sent by your partner" <<
            std::endl << std::endl << "For more information on a command " <<
            "use .help [command]" <<

            std::endl << std::endl;
    }
    else
    {
        if ((help.compare(0, 7, "encrypt") == 0) ||
            (help.compare(0, 8, ".encrypt") == 0))
        {
            std::cout << ".encrypt:" << std::endl <<
                "Encrypt a file for your chat partner using your " <<
                "key generated in this session." << std::endl << std::endl <<
                "Usage: .encrypt [File To Encrypt] " <<
                "[File To Save The Ciphertext]" << std::endl << std::endl;
        }
        else if ((help.compare(0, 7, "decrypt") == 0) ||
            (help.compare(0, 8, ".decrypt") == 0))
        {
            std::cout << ".decrypt:" << std::endl <<
                "Decrypt a file for your chat partner using your " <<
                "key generated in this session." << std::endl << std::endl <<
                "Usage: .encrypt [File To Decrypt] " <<
                "[File To Save The Plaintext]" << std::endl << std::endl;
        }
        else
        {
            std::cout << "Unkown help page: " << help << std::endl <<
                std::endl;
        }
    }
}

int aes_chat()
{
    /* Create our buffer values */
    aes_256_gcm_base64 client_local, client_remote;
    std::string input;
    base64 base64_buffer;

    bool valid_key = false;
    /* Keep looking until we get a valid local_client key */
    while (!valid_key)
    {
        /* State what we want to the user and grab the input */
        std::cout << "Enter your key (blank for new key): " << std::endl <<
            "> ";
        std::getline(std::cin, input);
        std::cout << std::endl;
        /* If we receive a blank answer,
         * generate a new key and exit this loop */
        if (input.length() == 0)
        {
            client_local.new_key();
            std::cout << "This is your key: " <<
                client_local.key_b64().string() << std::endl;
            valid_key = true;
        }
        /* If we receive something other than blank, try to base64 decode it
         * and generate a key with it, catching exceptions on whether or not
         * the key is actually valid */
        else
        {
            try
            {
                base64_buffer = input;
                client_remote.new_key(base64_buffer);
                valid_key = true;
                base64_buffer.clear();
            }
            catch (e_invalid_base64 const &e)
            {
                std::cerr << "Invalid key!" << std::endl;
            }
            catch (e_aes_invalid_key const &e)
            {
                std::cerr << "Invalid key!" << std::endl;
            }
        }
        /* Once we have a valid local_key enter the next loop looking for a
         * valid client_remote key */
        if (valid_key == true)
        {
            valid_key = false;
            std::cout << "Enter your partner's key: " << std::endl;
            while (!valid_key)
            {
                /* Get more user input for the client_remote key */
                std::cout << "> ";
                std::getline(std::cin, input);
                /* Try the client_remote key */
                try
                {
                    base64_buffer = input;
                    client_remote.new_key(base64_buffer);
                    valid_key = true;
                    base64_buffer.clear();
                }
                catch (e_invalid_base64 const &e)
                {
                    std::cerr << "Invalid key!";
                }
                catch (e_aes_invalid_key const &e)
                {
                    std::cerr << "Invalid key!";
                }
                std::cout << std::endl;
            }
        }
    }
    /* Initialize chat */
    std::cout <<
        "----Chat initialized----" << std::endl <<
        "Enter in your partner's message to decode or use .send [message] " <<
        "to encode your own message. Happy chatting!" << std::endl << std::endl;
    /* Exit tells us if we are safe to exit */
    bool exit = false;
    /* arg_string is used to send arguments to commands */
    std::string arg_string;
    while (!exit)
    {
        /* Get user input and deicde what to do with it */
        std::cout << "> ";
        std::getline(std::cin, input);
        std::cout << std::endl;
        if (input.compare(0,1, ".") == 0)
        {
            /* .quit will quit the program */
            if (input.compare(0, 5, ".quit") == 0)
            {
                exit = true;
            }
            /* .help displays help text */
            else if (input.compare(0, 5, ".help") == 0)
            {
                if (input.length() >= 7)
                {
                    aes_chat_help(input.substr(6));
                }
                else
                {
                    aes_chat_help("");
                }
            }
            /* .send will attempt to encrypt the string after and display it in
             * encrypted base64 text */
            else if (input.compare(0, 6, ".send ") == 0)
            {
                if (input.length() >= 7)
                {
                    arg_string = input.substr(6);
                    try
                    {
                        std::cout <<
                            client_remote.encrypt(arg_string).string() <<
                            std::endl << std::endl;
                    }
                    catch (e_aes_encryption_error const &e)
                    {
                        std::cerr << "Encryption error!" << std::endl;
                    }
                    catch (std::exception const &e)
                    {
                        std::cerr << "Exception caught: " << e.what() <<
                            std::endl;
                    }
                }
            }
            else if (input.compare(0, 9, ".encrypt ") == 0 ||
                input.compare(0, 9, ".decrypt ") == 0)
            {
                /* Proceduraly encrypt/decrypt based on which argument
                 * we received */
                bool encrypt = false;
                if (input.compare(0, 9, ".encrypt ") == 0)
                {
                    encrypt = true;
                }
                unsigned int it = 9;
                unsigned int len = input.length();
                bool quoted_path = false;
                /* If the path begins with a quote we read it differently */
                if (input.compare(9, 1, "\'")  == 0 ||
                    input.compare(9, 1, "\"") == 0)
                {
                    quoted_path = true;
                    it++;
                }
                bool end = false;
                /* The two arguments we eventually pass and a pointer */
                std::string file_read("");
                std::string file_write("");
                std::string *file = &file_read;
                while (!end)
                {
                    /* If we reached the end of input break out of the loop */
                    if (it >= len)
                    {
                        end = true;
                        break;
                    }
                    /* If we reached the end of the path break out */
                    if (quoted_path)
                    {
                        if (input.compare(it, 1, "\'") == 0 ||
                            input.compare(it, 1, "\"") == 0)
                        {
                            end = true;
                            it++;
                            break;
                        }
                    }
                    else
                    {
                        if (input.compare(it, 1, " ") == 0)
                        {
                            end = true;
                            break;
                        }
                    }
                    /* Otherwise add the character to our string and
                     * increment */
                    *file += input.at(it);
                    it++;
                }
                /* Ignore any spaces between paths */
                while (input.compare(it, 1, " ") == 0)
                {
                    it++;
                }
                /* If by then we reached the end then we know that the
                 * second path does not exist */
                if (it >= len)
                {
                    std::cerr << "Unknown second argument provided!" <<
                        std::endl << std::endl;
                }
                else
                {
                    /* Reset and set up for the second argument */
                    end = false;
                    file = &file_write;
                    if (input.compare(9, 1, "\'")  == 0 ||
                        input.compare(9, 1, "\"") == 0)
                    {
                        quoted_path = true;
                        it++;
                    }
                    else
                    {
                        quoted_path = false;
                    }
                    while (!end)
                    {
                        /* If we reached the end of input break out of the
                         * loop */
                        if (it >= len)
                        {
                            end = true;
                            break;
                        }
                        /* If we reached the end of the path break out */
                        if (quoted_path)
                        {
                            if (input.compare(it, 1, "\'") == 0 ||
                                input.compare(it, 1, "\"") == 0)
                            {
                                end = true;
                                it++;
                                break;
                            }
                        }
                        else
                        {
                            if (input.compare(it, 1, " ") == 0)
                            {
                                end = true;
                                break;
                            }
                        }
                        /* Otherwise add the character to our string and
                         * increment */
                        *file += input.at(it);
                        it++;
                    }
                    /* Encrypt or decrypt proceduraly */
                    if (encrypt)
                    {
                        try
                        {
                            file_encrypt(client_remote, file_read, file_write);
                            std::cout << "Encryption successful!" <<
                                std::endl << std::endl;
                        }
                        catch (std::logic_error const &e)
                        {
                            std::cout << "Unable to encrypt file! Error: " <<
                                e.what() << std::endl << std::endl;
                        }
                    }
                    else
                    {
                        try
                        {
                            file_decrypt(client_local, file_read, file_write);
                            std::cout << "Decryption successful!" <<
                                std::endl << std::endl;
                        }
                        catch (std::logic_error const &e)
                        {
                            std::cout << "Unable to decrypt file! Error: " <<
                                e.what() << std::endl << std::endl;
                        }
                    }
                }
            }
            /* Command prefix but unknown command triggers an error */
            else
            {
                std::cerr << "Invalid command: " << input << std::endl <<
                    std::endl;
            }
        }
        /* If we receive a string of text without the command prefix, assume
         * it's a message to decrypt */
        else
        {
            try
            {
                base64_buffer = input;
                std::cout << client_local.decrypt(base64_buffer) <<
                    std::endl << std::endl;
                base64_buffer.clear();
            }
            catch(e_aes_decryption_error const &e)
            {
                std::cerr << "Decryption error!" << std::endl;
            }
            catch(std::exception const &e)
            {
                std::cerr << "Exception caught: " << e.what() << std::endl;
            }
        }
    }
    return 0;
}

int initialize_chat(void)
{
    std::string input;
    std::cout << "Welcome to Haffy Chat! Please choose from one of the " <<
        "fllowing chat options:" << std::endl <<
        "[1] curve25519 Exchange + AES-256-GCM" << std::endl <<
        "[2] AES-256-GCM" << std::endl;
    while (true)
    {
        std::cout << "> ";
        std::getline(std::cin, input);
        if (input.compare(0, 1, "1") == 0)
        {
            std::cout << std::endl;
            return curve25519_aes_chat();
        }
        if (input.compare(0, 1, "2") == 0)
        {
            std::cout << std::endl;
            return aes_chat();
        }
        else
        {
            std::cout << "Invalid choice." << std::endl;
        }
    }
}
