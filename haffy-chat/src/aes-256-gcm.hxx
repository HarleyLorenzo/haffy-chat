#ifndef AES_256_GCM_HXX
# define AES_256_GCM_HXX

#include "base64.hxx"
#include <array>
#include <exception>
#include <vector>
#include <sodium.h>

typedef std::array<unsigned char, crypto_aead_aes256gcm_KEYBYTES>
    aes_256_gcm_key;
typedef std::array<unsigned char, crypto_aead_aes256gcm_NPUBBYTES>
    aes_256_gcm_nonce;

class e_aes: public std::exception
{
public:
    virtual const char* what() const noexcept =0;
};

class e_aes_decryption_error: public e_aes
{
public:
    virtual const char* what() const noexcept;
};

class e_aes_encryption_error: public e_aes
{
public:
    virtual const char* what() const noexcept;
};

class e_aes_invalid_key: public e_aes
{
public:
    virtual const char* what() const noexcept;
};

class aes_256_gcm
{
protected:
    aes_256_gcm_key key;
    aes_256_gcm_nonce nonce;
    crypto_aead_aes256gcm_state ctx;

    void expand_key(void);
    void increment_nonce(void);
public:
    std::vector<unsigned char> decrypt(std::vector<unsigned char> const &);
    std::vector<unsigned char> encrypt(std::vector<unsigned char> const &);
    void new_key(void);
    void new_key(aes_256_gcm_key const &);
    void swap(aes_256_gcm const &) noexcept;
};

class aes_256_gcm_base64: public aes_256_gcm
{
public:
    using aes_256_gcm::decrypt;
    using aes_256_gcm::encrypt;
    using aes_256_gcm::new_key;

    std::string   decrypt(base64 const &);
    base64 encrypt(std::string const &);
    base64 key_b64(void) const;
    void new_key(base64 const &);
};

#endif /* AES_256_GCM_HXX */
