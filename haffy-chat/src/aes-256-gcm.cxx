#include "aes-256-gcm.hxx"
#include <algorithm>
#include <exception>
#include <iterator>
#include <string>
#include <utility>
#include <vector>
#include <sodium.h>

/*** Exceptions ***/

const char* e_aes_decryption_error::what() const noexcept
{
    return "AES decryption failed";
}

const char* e_aes_encryption_error::what() const noexcept
{
    return "AES encryption failed";
}

const char* e_aes_invalid_key::what() const noexcept
{
    return "Invalid key provided";
}

/*** aes_256_gcm ***/

/* Member Functions */
void aes_256_gcm::expand_key(void)
{
    /* Expand our key for the precomputation interface */
    crypto_aead_aes256gcm_beforenm(&ctx, key.data());
}

void aes_256_gcm::increment_nonce(void)
{
    /* Increment the first byte of the nonce from end to beginning that isn't
     * already at the max value, restting them all to 0 if we max out */
    for (auto i = nonce.end() -1; i > nonce.begin() -1; i--)
    {
        if (i == nonce.begin())
        {
            if (*i != 255)
            {
                *i += 1;
                break;
            }
            else
            {
                for (auto it = nonce.begin(); i < nonce.end(); i++)
                {
                    *it = 0;
                }
                break;
            }

        }
        else if (*i != 255)
        {
            *i += 1;
            break;
        }
    }
}

std::vector<unsigned char> aes_256_gcm::decrypt(
    std::vector<unsigned char> const &bstream)
{
    /* Generate our return value with the maximum size the decryption can be */
    std::vector<unsigned char> ret (bstream.size() -
        crypto_aead_aes256gcm_NPUBBYTES - crypto_aead_aes256gcm_ABYTES);
    /* Used to check later if additional data was used and caused additional
     * memory to be allocated */
    unsigned long long size;
    /* Copy the nonce over */
    std::copy(bstream.cbegin(),
        bstream.cbegin() + crypto_aead_aes256gcm_NPUBBYTES, nonce.begin());
    /* Decrypt and throw an exception if it fails */
    if (crypto_aead_aes256gcm_decrypt_afternm(ret.data(), &size, NULL,
        bstream.data() + crypto_aead_aes256gcm_NPUBBYTES, bstream.size() -
        crypto_aead_aes256gcm_NPUBBYTES, NULL, 0, nonce.data(), &ctx) != 0)
    {
        throw(e_aes_decryption_error());
    }
    else
    {
        /* If we did allocate additional memory, deallocate it */
        ret.resize(size);
        return ret;
    }
}

std::vector<unsigned char> aes_256_gcm::encrypt(
    std::vector<unsigned char> const &bstream)
{
    /* Generate a return vector that is the correct size as well as place the
     * nonce in the beginning of its bytespace */
    std::vector<unsigned char> ret(bstream.size() +
        crypto_aead_aes256gcm_NPUBBYTES + crypto_aead_aes256gcm_ABYTES);
    std::copy(nonce.cbegin(), nonce.cend(), ret.begin());
    /* Encrypt and throw an exception if it does not work */
    if (crypto_aead_aes256gcm_encrypt_afternm(ret.data() +
        crypto_aead_aes256gcm_NPUBBYTES, NULL, bstream.data(), bstream.size(),
        NULL, 0, NULL, nonce.data(), &ctx) != 0)
    {
        throw(e_aes_encryption_error());
    }
    else
    {
        increment_nonce();
        return ret;
    }
}

void aes_256_gcm::new_key(void)
{
    /* Randomly generate a new key and nonce */
    crypto_aead_aes256gcm_keygen(key.data());
    randombytes_buf(nonce.data(), crypto_aead_aes256gcm_NPUBBYTES);
    expand_key();
}

void aes_256_gcm::new_key(aes_256_gcm_key const &new_key)
{
    /* Create a new key from a bytestream passed to us and generate a new
     * nonce with it */
    randombytes_buf(nonce.data(), crypto_aead_aes256gcm_NPUBBYTES);
    std::copy(new_key.cbegin(), new_key.cend(), key.begin());
    expand_key();
}

void aes_256_gcm::swap(aes_256_gcm const &o) noexcept
{
    auto tmp(o);
    std::swap(key, tmp.key);
    std::swap(nonce, tmp.nonce);
    std::swap(ctx, tmp.ctx);
}

/*** aes_256_gcm_base64 ***/

/* Member Functions */
std::string aes_256_gcm_base64::decrypt(base64 const &message_b64)
{
    /* message_bstream is where we place the message_bstream text */
    std::vector<unsigned char> message_bstream;
    /* Get our decoded base64_buffer */
    try
    {
        message_bstream = message_b64.decode();
    }
    catch (...)
    {
        /* TODO: Inherited class with more info */
        throw(e_aes_decryption_error());
    }
    /* If the message is not even long enough to be a proper encrypted message,
     * don't even try to decrypt */
    if (message_bstream.size() <=
        (crypto_aead_aes256gcm_NPUBBYTES + crypto_aead_aes256gcm_ABYTES + 1))
    {
        /* TODO: Inherited class with more info */
        throw(e_aes_decryption_error());
    }
    /* Construct our return len, our ret of highest possible decrypted size
     * and also copy nonce into our class */
    unsigned long long message_len;
    std::string ret;
    ret.resize(message_bstream.size() - crypto_aead_aes256gcm_ABYTES);
    std::copy(message_bstream.cbegin(),  message_bstream.cbegin() +
        crypto_aead_aes256gcm_NPUBBYTES, nonce.begin());
    /* Try decryption */
    if (crypto_aead_aes256gcm_decrypt_afternm(
        (unsigned char *)ret.data(), &message_len, NULL,
        message_bstream.data() + crypto_aead_aes256gcm_NPUBBYTES,
        message_bstream.size() - crypto_aead_aes256gcm_NPUBBYTES,
        NULL, 0, nonce.data(), &ctx) != 0)
    /* And if it errors, throw an exception */
    {
        throw(e_aes_decryption_error());
    }
    /* If our ret is larger than what the content actually was then resize
     * lower. This can happen because of additional byte data */
    ret.resize(message_len);

    return ret;
}

base64 aes_256_gcm_base64::encrypt(std::string const &message_s)
{
    /* construct our ret and also our buffer that we will encode, placing the
     * nonce at the beginning first */
    base64 ret;
    std::vector<unsigned char> buffer (message_s.size() +
        crypto_aead_aes256gcm_NPUBBYTES + crypto_aead_aes256gcm_ABYTES);
    std::copy(nonce.cbegin(), nonce.cend(), buffer.begin());
    /* Encrypt, throwing if it fails */
    if (crypto_aead_aes256gcm_encrypt_afternm(buffer.data() +
        crypto_aead_aes256gcm_NPUBBYTES, NULL,
        (unsigned char *)message_s.data(), message_s.size(),
        NULL, 0, NULL, nonce.data(), &ctx) != 0)
    /* And if it fails, throw an exception */
    {
        throw(e_aes_encryption_error());
    }
    /* If it doesn't fail, continue */
    else
    {
        try
        {
            ret.encode(buffer);
        }
        catch (...)
        {
            /* TODO: Inherited class with more info */
            throw(e_aes_encryption_error());
        }
    }
    increment_nonce();
    return ret;
}

base64 aes_256_gcm_base64::key_b64(void) const
{
    /* Create a vector bytestream to pass to the encoder, then encode the
     * byestream and return the encode */
    std::vector<unsigned char> key_bstream (crypto_aead_aes256gcm_KEYBYTES);
    base64 key_b64;
    std::copy(key.cbegin(), key.cend(), key_bstream.begin());
    key_b64.encode(key_bstream);
    return key_b64;
}

void aes_256_gcm_base64::new_key(base64 const &key_b64)
{
    /* Decode key_b64 into key_bstream, checking length, copying the
     * new key, generating a new nonce then copying it into our nonce. */
    std::vector<unsigned char> key_bstream = key_b64.decode();
    if (key_bstream.size() != crypto_aead_aes256gcm_KEYBYTES)
    {
        throw(e_aes_invalid_key());
    }
    else
    {
        std::copy(key_bstream.cbegin(), key_bstream.cend(), key.begin());
    }
    randombytes_buf(nonce.data(), crypto_aead_aes256gcm_NPUBBYTES);
    expand_key();
}
