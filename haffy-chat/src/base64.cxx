/* base64.cxx contains base64 encoding, decoding and checking functions */

#include "base64.hxx"
#include <exception>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

/*** Exceptions ***/

const char* e_invalid_base64::what() const noexcept
{
    return "Invalid base64 string";
}

/*** base64 ***/

base64 base64::operator=(std::string const &o) noexcept
{
    encoded_text = o;
    return *this;
}

void base64::clear(void)
{
    encoded_text.clear();
}

/* Member Functions */
std::vector<unsigned char> base64::decode(void) const
{
    /* len so we know how long to loop for */
    unsigned int len = encoded_text.length();
    /* Iterator we use so we know how many bytes we've processed */
    unsigned int i = 0;
    /* Iterator for how deep into encoded_text we go */
    unsigned int in = 0;
    /* Buffer which we place the decoded chars */
    unsigned char decoded_buffer[3];
    /* Buffer which we place the encoded chars */
    unsigned char encoded_buffer[4];
    /* std::string we return at the end placing characters in */
    std::vector<unsigned char> ret;

    /* Iterate encoded_text fully throwing if there is a nonbase64 */
    while(len-- && encoded_text[in] != '=')
    {
        /* Place four characters into encoded_buffer */
        if (!is_base64(encoded_text[in]))
        {
            throw(e_invalid_base64());
        }
        encoded_buffer[i++] = encoded_text[in++];
        if (i == 4)
        {
            /* Replace our encoded_buffer with the binary it represents */
            for (i = 0; i < 4; i++)
            {
                encoded_buffer[i] = base64_chars.find(encoded_buffer[i]);
            }
            /* Now decode it back into 3 8 byte characters */
            decoded_buffer[0] = (encoded_buffer[0] << 2) +
                ((encoded_buffer[1] & 0x30) >> 4);
            decoded_buffer[1] = ((encoded_buffer[1] & 0xf) << 4) +
                ((encoded_buffer[2] & 0x3c) >> 2);
            decoded_buffer[2] = ((encoded_buffer[2] & 0x3) << 6) +
                encoded_buffer[3];
            /* Place those characters into ret */
            for (i = 0; i < 3; i++)
            {
                ret.push_back(decoded_buffer[i]);
            }
            /* Reset our iterator */
            i = 0;
        }
    }
    /* If there are leftover (meaning not a whole number of 4) bytes... */
    if (i)
    {
        unsigned int j;
        /* Replace what we have with its binary equalivant */
        for (j = 0; j < i; j++)
        {
            encoded_buffer[j] = base64_chars.find(encoded_buffer[j]);
        }
        /* Convert what we have into decoded bytes */
        decoded_buffer[0] = (encoded_buffer[0] << 2) +
            ((encoded_buffer[1] & 0x30) >> 4);
        decoded_buffer[1] = ((encoded_buffer[1] & 0xf) << 4) +
            ((encoded_buffer[2] & 0x3c) >> 2);
        /* Add them to our return value */
        for (j = 0; j < i - 1; j++)
        {
                        ret.push_back(decoded_buffer[j]);
        }
    }
    return ret;
}

void base64::encode(std::vector<unsigned char> const &bytestream)
{
    /* len for how many bytes we have left to process */
    unsigned int len = bytestream.size();
    /* Iterators we use so we know how many bytes we've processed */
    unsigned int i = 0;
    unsigned int j = 0;
    /* Buffer that we store characters we need to encode */
    char decoded_buffer[3];
    /* Buffer that we store characters we have encoded */
    char encoded_buffer[4];
    /* Iterate until there are no bytes left */
    while (len--)
    {
        /* Store bytes into decoded_buffer until we have 3 */
        decoded_buffer[i] = static_cast<char>(bytestream.at(j));
        i++;
        j++;
        if (i == 3)
        {
            /* Convert them to 4 6-byte characters */
            encoded_buffer[0] = (decoded_buffer[0] & 0xfc) >> 2;
            encoded_buffer[1] = ((decoded_buffer[0] & 0x03) << 4) +
                ((decoded_buffer[1] & 0xf0) >> 4);
            encoded_buffer[2] = ((decoded_buffer[1] & 0x0f) << 2) +
                ((decoded_buffer[2] & 0xc0) >> 6);
            encoded_buffer[3] = decoded_buffer[2] & 0x3f;
            /* Encode and store them in base64 */
            for(i = 0; i < 4; i++)
            {
                encoded_text += base64_chars[encoded_buffer[i]];
            }
            /* Reset our iterator if we finished 3 bytes */
            i = 0;
        }
    }
    /* If we didn't finish a full 3 characters */
    if (i)
    {
        /* Fill up any leftover bytes with 0x00 */
        for (j = i; j < 3; j++)
        {
            encoded_buffer[j] = '\0';
            }
        /* Convert them to base64 */
        encoded_buffer[0] = (decoded_buffer[0] & 0xfc) >> 2;
        encoded_buffer[1] = ((decoded_buffer[0] & 0x03) << 4) +
            ((decoded_buffer[1] & 0xf0) >> 4);
        encoded_buffer[2] = ((decoded_buffer[1] & 0x0f) << 2) +
            ((decoded_buffer[2] & 0xc0) >> 6);
        /* Store what encoded text we do have */
        for (j = 0; j < i + 1; j++)
        {
            encoded_text += base64_chars[encoded_buffer[j]];
        }
        /* Pad the rest with "=" */
        while (i++ < 3)
        {
            encoded_text += '=';
        }
    }
}

void base64::encode(unsigned char const *bytestream, unsigned int len)
{
    /* Iterator we use so we know how many bytes we've processed */
        unsigned int i = 0;
    /* Buffer that we store characters we need to encode */
    char decoded_buffer[3];
    /* Buffer that we store characters we have encoded */
    char encoded_buffer[4];
    /* Iterate until there are no bytes left */
    while (len--)
    {
        /* Store bytes into decoded_buffer until we have 3 */
        decoded_buffer[i++] = *(bytestream++);
        if (i == 3)
        {
            /* Convert them to 4 6-byte characters */
            encoded_buffer[0] = (decoded_buffer[0] & 0xfc) >> 2;
            encoded_buffer[1] = ((decoded_buffer[0] & 0x03) << 4) +
                    ((decoded_buffer[1] & 0xf0) >> 4);
            encoded_buffer[2] = ((decoded_buffer[1] & 0x0f) << 2) +
                    ((decoded_buffer[2] & 0xc0) >> 6);
            encoded_buffer[3] = decoded_buffer[2] & 0x3f;
            /* Encode and store them in base64 */
            for(i = 0; i < 4; i++)
            {
                encoded_text += base64_chars[encoded_buffer[i]];
            }
            /* Reset our iterator if we finished 3 bytes */
            i = 0;
        }
    }
    /* If we didn't finish a full 3 characters */
    if (i)
    {
        /* Fill up any leftover bytes with 0x00 */
        unsigned int j = 0;
        for (j = i; j < 3; j++)
        {
            encoded_buffer[j] = '\0';
        }
        /* Convert them to base64 */
        encoded_buffer[0] = (decoded_buffer[0] & 0xfc) >> 2;
        encoded_buffer[1] = ((decoded_buffer[0] & 0x03) << 4) +
            ((decoded_buffer[1] & 0xf0) >> 4);
        encoded_buffer[2] = ((decoded_buffer[1] & 0x0f) << 2) +
            ((decoded_buffer[2] & 0xc0) >> 6);
        /* Store what encoded text we do have */
        for (j = 0; j < i + 1; j++)
        {
            encoded_text += base64_chars[encoded_buffer[j]];
        }
        /* Pad the rest with "=" */
        while (i++ < 3)
        {
            encoded_text += '=';
        }
    }
}

/* Returns the string */
std::string base64::string() const noexcept
{
    return encoded_text;
}

/*** Functions ***/

/* Test if a character is base64 or not */
bool inline is_base64(unsigned char character)
{
    return (isalnum(character) || (character == '=') || (character == '+') ||
        (character == '/'));
}
