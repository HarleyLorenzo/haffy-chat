#ifndef BASE64_H
#   define BASE64_H

#include <exception>
#include <string>
#include <vector>

class e_base64: public std::exception
{
public:
    virtual const char* what() const noexcept =0;
};

class e_invalid_base64: public e_base64
{
public:
    virtual const char* what() const noexcept;
};

class base64
{
public:
    std::string encoded_text;
    std::string const base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";

public:
    base64 operator=(std::string const &) noexcept;

    void clear(void);
    std::vector<unsigned char> decode(void) const;
    void encode(std::vector<unsigned char> const &);
    void encode(unsigned const char *, unsigned int);
    std::string string(void) const noexcept;
};

bool is_base64(unsigned const char);

#endif /* BASE64_H */
