/* The header file for haffy-chat.cxx */
#ifndef HAFFY_CHAT_H
#   define HAFFY_CHAT_H

#include <array>
#include <string>
#include "haffy-chat-config.h"
#include "base64.hxx"

/* Macros to convert our version config define into a C++ string */
#define STR_(X) #X
#define STR(X)  STR_(X)


namespace global
{
    /* Our version string using the above preprocessor macro */
    std::string const version =
        STR(HAFFY_CHAT_VERSION_MAJOR)   "."
        STR(HAFFY_CHAT_VERSION_MINOR)   "."
        STR(HAFFY_CHAT_VERSION_RELEASE)
        STR(HAFFY_CHAT_VERSION_APPEND);

    enum enumBoolFlags
    {
        verbose
    };

    enum enumIntFlags
    {

    };
    std::array<bool, 1> boolFlags;
    std::array<int, 1>  intFlags;
}

#endif /* HAFFY_CHAT_H */
