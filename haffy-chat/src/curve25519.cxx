#include "curve25519.hxx"
#include <algorithm>
#include <array>
#include <exception>
#include <sodium.h>

/*** Exceptions ***/

const char * e_curve25519_invalid_public::what() const noexcept
{
    return "Invalid public key for session key creation";
}

/*** curve25519 ***/

/* Member Functions */
curve25519_public_key curve25519::get_local_key_public(void) const
{
    return local_key_public;
}

void curve25519::new_local_keys (void) noexcept
{
    /* Generate curve25519 keys randomly */
    unsigned char c_local_key_secret[crypto_kx_SECRETKEYBYTES];
    unsigned char c_local_key_public[crypto_kx_PUBLICKEYBYTES];
    crypto_kx_keypair(c_local_key_public, c_local_key_secret);
    std::copy(c_local_key_secret, c_local_key_secret + crypto_kx_SECRETKEYBYTES,
        local_key_secret.begin());
    std::copy(c_local_key_public, c_local_key_public + crypto_kx_PUBLICKEYBYTES,
        local_key_public.begin());
}

std::array<unsigned char, crypto_kx_SESSIONKEYBYTES*2>
    curve25519::session_keys (curve25519 const &remote) const
{
    /* Create all the unsigned chars needed for our session key function
     * and remote_key_public */
    unsigned char c_local_key_secret[crypto_kx_SECRETKEYBYTES];
    unsigned char c_local_key_public[crypto_kx_PUBLICKEYBYTES];
    unsigned char c_remote_key_public[crypto_kx_PUBLICKEYBYTES];
    unsigned char local_key[crypto_kx_SESSIONKEYBYTES];
    unsigned char remote_key[crypto_kx_SESSIONKEYBYTES];
    curve25519_public_key remote_key_public = remote.get_local_key_public();
    /* Copy our keys over to the key varriables */
    std::copy(local_key_secret.cbegin(), local_key_secret.cend(),
        c_local_key_secret);
    std::copy(local_key_public.cbegin(), local_key_public.cend(),
        c_local_key_public);
    std::copy(remote_key_public.cbegin(), remote_key_public.cend(),
        c_remote_key_public);
    /* Generate our tx key and our rx key using these two separate functions */
    if (crypto_kx_server_session_keys(NULL, remote_key,
        c_local_key_public, c_local_key_secret, c_remote_key_public) != 0)
    {
        throw(e_curve25519_invalid_public());
    }
    if (crypto_kx_client_session_keys(local_key, NULL,
        c_local_key_public, c_local_key_secret, c_remote_key_public) != 0)
    {
        throw(e_curve25519_invalid_public());
    }
    /* Copy our keys to ret then return ret */
    std::array<unsigned char, crypto_kx_SESSIONKEYBYTES*2> ret;
    std::copy(local_key, local_key + crypto_kx_SESSIONKEYBYTES,
        ret.begin());
    std::copy(remote_key, remote_key + crypto_kx_SESSIONKEYBYTES,
        ret.begin() + crypto_kx_SESSIONKEYBYTES);
    return ret;
}

void curve25519::set_local_key_public(curve25519_public_key const &new_key)
{
    /* Set a new remote key */
    local_key_public = new_key;
}

void curve25519::swap(curve25519 const &o) noexcept
{
    auto tmp(o);
    std::swap(local_key_secret, tmp.local_key_secret);
    std::swap(local_key_public, tmp.local_key_public);
}
